package frc.robot.subsystems;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.path.PathPlannerPath;

import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;

public class RunnablePath {

    PathPlannerPath path;
    boolean run;
    ShuffleboardTab shuffleBoardTab;

    public RunnablePath(int index) {
        String key = "Note"+index;
        System.out.println(key);
        path = PathPlannerPath.fromPathFile(key);
    }

    public Command runPath() {
        return AutoBuilder.pathfindThenFollowPath(path, Constants.Constraints);
    }

    public boolean desired() {
        return run;
    }

    public void setDesired(boolean value) {
        run = value;
    }

}
