package frc.robot.subsystems.elevator;

import org.littletonrobotics.junction.Logger;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.elevator.ElevatorIO.ElevatorIOInputsAutoLogged;
import frc.robot.Constants;
import frc.robot.subsystems.elevator.ElevatorIO.ElevatorIO;

public class ElevatorSubsystem extends SubsystemBase {
    
    private final ElevatorIO m_io;
    private ElevatorIOInputsAutoLogged m_inputs = new ElevatorIOInputsAutoLogged();

    public ElevatorSubsystem(ElevatorIO io) {
        m_io = io;
        initialize();
    }


    protected void initialize() {
        m_io.setElevatorMotorSpeed(0);
    }

    public void elevatorDown() {
        m_io.setElevatorMotorSpeed(-Constants.ElevatorConstants.ELEVATOR_SPEED);
    }

    public void elevatorUp() {
        m_io.setElevatorMotorSpeed(Constants.ElevatorConstants.ELEVATOR_SPEED);
    }

    public void stopElevator() {
        m_io.setElevatorMotorSpeed(0);
    }

    public Rotation2d getAngle(){
        return Rotation2d.fromDegrees(m_inputs.elevatorDistance);
    }


    @Override
    public void periodic() {
        m_io.updateInputs(m_inputs);
         Logger.processInputs("Inputs/Elevator", m_inputs);
    }
}
