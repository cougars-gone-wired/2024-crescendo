package frc.robot.subsystems.elevator;

import frc.robot.Constants;
import frc.robot.subsystems.elevator.ElevatorIO.ElevatorIOReal;
import frc.robot.subsystems.elevator.ElevatorIO.ElevatorIOSim;

public class ElevatorSubsystemFactory {
    public static ElevatorSubsystem build(Constants constants) {
        if (constants.Elevator.ENABLED) {
            return new ElevatorSubsystem(new ElevatorIOReal(constants));
        } else {
            return new ElevatorSubsystem(new ElevatorIOSim());
        }        
    }
}
