// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
package frc.robot.subsystems.elevator.ElevatorIO;

import frc.robot.sim.MotorSim;

public class ElevatorIOSim implements ElevatorIO {
  private final MotorSim leftMotor = new MotorSim(60);
  private final MotorSim rightMotor = new MotorSim(60);
  /**
   * Updates the set of loggable inputs.
   */
   public void updateInputs(ElevatorIOInputs inputs) {
    inputs.elevatorSpeed = leftMotor.get();
    inputs.elevatorDistance = leftMotor.getDistance();
    leftMotor.periodic();
    rightMotor.periodic();
    //possibly add elevator  position here after KCMT
  }

  public void moveElevatorToPos(double elevatorSpeed, double elevatorPosition, double desiredElevatorPosition) {
    //Will not be implemented before KCMT.
  }

  public void setElevatorMotorSpeed(double elevatorSpeed){
    leftMotor.set(elevatorSpeed);
    rightMotor.set(elevatorSpeed);
  }
}