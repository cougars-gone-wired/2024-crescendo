// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.
package frc.robot.subsystems.elevator.ElevatorIO;

import com.ctre.phoenix6.controls.VelocityVoltage;
import com.ctre.phoenix6.hardware.TalonFX;
import com.ctre.phoenix6.signals.NeutralModeValue;
import com.ctre.phoenix6.configs.TalonFXConfiguration;

import frc.robot.Constants;

public class ElevatorIOReal implements ElevatorIO {

  private final TalonFX leftMotor;
  private final TalonFX rightMotor;


  public ElevatorIOReal(Constants constants) {
    leftMotor = new TalonFX(constants.Elevator.LEFT_MOTOR_ID); //Left motor exists yay
    rightMotor = new TalonFX(constants.Elevator.RIGHT_MOTOR_ID); //Right motor exists yay

    TalonFXConfiguration config = new TalonFXConfiguration(); //Creates config class

    config.MotorOutput.NeutralMode = NeutralModeValue.Brake; //sets the elevator motor mode to "Break" (instant stopping)
    config.CurrentLimits.StatorCurrentLimitEnable = true; //Enables stator current limits
    config.CurrentLimits.StatorCurrentLimit = 40; //Sets stator current limit
    config.SoftwareLimitSwitch.ForwardSoftLimitEnable = true; //Enables encoder limit
    config.SoftwareLimitSwitch.ForwardSoftLimitThreshold = 20; //Sets an UNTESTED encoder upper limit
    config.SoftwareLimitSwitch.ReverseSoftLimitEnable = true;
    config.SoftwareLimitSwitch.ReverseSoftLimitThreshold = 0;
    

    leftMotor.getConfigurator().apply(config); //Applies configs (above) to the left motor
    rightMotor.getConfigurator().apply(config); //Applies configs (above) to the right motor
  
    leftMotor.setInverted(false); //Sets left motor's default rotating direction to be inverted of the normal (which we CURRENTLY DONT KNOW)
    rightMotor.setInverted(true); //Sets right motor's deafault rotating direction to be normal (which we CURRENTLY DONT KNOW)
  }


  /**
   * Updates the set of loggable inputs.
   */
  public void updateInputs(ElevatorIOInputs inputs) {
    inputs.elevatorSpeed = leftMotor.get();
    inputs.elevatorDistance = 0;
    inputs.elevatorPosition = rightMotor.getPosition().getValueAsDouble(); //Gets encoder value (based off of right motor)
    //possibly add elevator  position here after KCMT
  }

  public void moveElevatorToPos(double elevatorSpeed, double elevatorPosition, double desiredElevatorPosition) {
    //Will not be implemented before KCMT.
  }

  public void setElevatorMotorSpeed(double elevatorSpeed){
    leftMotor.set(elevatorSpeed);
    rightMotor.set(elevatorSpeed);
  }

}