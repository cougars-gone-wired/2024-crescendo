// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.elevator.ElevatorIO;

import org.littletonrobotics.junction.AutoLog;



/**
 * elevator subsystem hardware interface.
 */
public interface ElevatorIO {

  /**
   * Contains all of the input data received from hardware.
   */
  @AutoLog
  public static class ElevatorIOInputs {
   // public boolean lowerLimit = false;
    public double elevatorSpeed = 0;
    //public double elevatorPosition = 0;
    public double elevatorDistance = 0;
    public double elevatorPosition = 0;
  }

  /**
   * Updates the set of loggable inputs.
   */
  public void updateInputs(ElevatorIOInputs inputs);

  /**
   * Set motors to specified speed.
   */
  public void moveElevatorToPos(double elevatorSpeed, double elevatorPosition, double desiredElevatorPosition);
  public void setElevatorMotorSpeed(double elevatorSpeed);

}