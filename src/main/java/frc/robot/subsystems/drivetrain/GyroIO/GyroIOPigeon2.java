package frc.robot.subsystems.drivetrain.GyroIO;

import com.ctre.phoenix6.hardware.Pigeon2;

public class GyroIOPigeon2 implements GyroIO {
    private final Pigeon2 m_gyro = new Pigeon2(0);

    @Override
    public void updateInputs(GyroIOInputs inputs, double angularVelocity) {
        inputs.yawDegreesCCW = m_gyro.getRotation2d();
    }
}
