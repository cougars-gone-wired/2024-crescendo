// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.GyroIO;

import org.littletonrobotics.junction.Logger;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.SPI;

public class GyroIONavxAd450 implements GyroIO {
  private final AHRS navx = new AHRS(SPI.Port.kMXP, (byte) 200); // NavX connected over MXP
  private final ADXRS450_Gyro ad450 = new ADXRS450_Gyro(); 
  private boolean failover = false;
  private Rotation2d offset = new Rotation2d(0);

  @Override
  public void updateInputs(GyroIOInputs inputs, double angularVelocity) {
    Logger.recordOutput("Gyro/NAVX isConnected", navx.isConnected());
    if(failover || !navx.isConnected()) {
      failover = true;
      inputs.yawDegreesCCW = ad450.getRotation2d().plus(offset);
    }
    inputs.yawDegreesCCW = Rotation2d.fromDegrees(-navx.getYaw());
    // convert to ccw positive
    offset = inputs.yawDegreesCCW.minus(ad450.getRotation2d());
  }
}
