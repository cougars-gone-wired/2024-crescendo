// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.GyroIO;

import org.littletonrobotics.junction.Logger;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.ADIS16470_IMU;
import edu.wpi.first.wpilibj.SPI;

public class GyroIONavxAd16470 implements GyroIO {
  private final AHRS navx = new AHRS(SPI.Port.kMXP, (byte) 200); // NavX connected over MXP
  private final ADIS16470_IMU ad16470 = new ADIS16470_IMU(); 
  private boolean failover = false;
  private Rotation2d offset = new Rotation2d(0);

  @Override
  public void updateInputs(GyroIOInputs inputs, double angularVelocity) {
    Logger.recordOutput("Gyro/NAVX isConnected", navx.isConnected());
    if(failover || !navx.isConnected()) {
      failover = true;
      inputs.yawDegreesCCW = Rotation2d.fromDegrees(ad16470.getAngle()).plus(offset);
    }
    inputs.yawDegreesCCW = Rotation2d.fromDegrees(-navx.getYaw());
    // convert to ccw positive
    offset = inputs.yawDegreesCCW.minus(Rotation2d.fromDegrees(ad16470.getAngle()));
  }
}
