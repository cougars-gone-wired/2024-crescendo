// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.GyroIO;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;

public class GyroIOADXRS450 implements GyroIO {
  private final ADXRS450_Gyro gyro = new ADXRS450_Gyro(); 

  @Override
  public void updateInputs(GyroIOInputs inputs, double angularVelocity) {
    inputs.yawDegreesCCW = Rotation2d.fromDegrees(gyro.getAngle());
  }
}
