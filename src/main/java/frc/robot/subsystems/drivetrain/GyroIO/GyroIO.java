// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.GyroIO;

import org.littletonrobotics.junction.AutoLog;

import edu.wpi.first.math.geometry.Rotation2d;

/**
 * climber subsystem hardware interface.
 */
public interface GyroIO {
  /**
   * Contains all of the input data received from hardware.
   */
  @AutoLog
  public static class GyroIOInputs {
    public Rotation2d yawDegreesCCW = new Rotation2d(0);
  }

  /**
   * Updates the set of loggable inputs.
   */
  public default void updateInputs(GyroIOInputs inputs, double angularVelocity) {}
}
