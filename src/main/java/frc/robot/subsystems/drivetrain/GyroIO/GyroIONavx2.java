// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.GyroIO;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.SPI;

public class GyroIONavx2 implements GyroIO {
  private final AHRS navx = new AHRS(SPI.Port.kMXP, (byte) 200); // NavX connected over MXP

  @Override
  public void updateInputs(GyroIOInputs inputs, double angularVelocity) {
    inputs.yawDegreesCCW = Rotation2d.fromDegrees(-navx.getYaw());
  }
}
