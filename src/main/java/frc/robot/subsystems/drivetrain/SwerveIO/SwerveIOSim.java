// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.SwerveIO;

import frc.robot.Constants;
import frc.robot.sim.SwerveModuleSim;

public class SwerveIOSim extends SwerveIO {
  public SwerveIOSim() {
    frontLeftModule = new SwerveModuleSim(Constants.MAX_VELOCITY_METERS_PER_SECOND);
    frontRightModule = new SwerveModuleSim(Constants.MAX_VELOCITY_METERS_PER_SECOND);
    backLeftModule = new SwerveModuleSim(Constants.MAX_VELOCITY_METERS_PER_SECOND);
    backRightModule = new SwerveModuleSim(Constants.MAX_VELOCITY_METERS_PER_SECOND);
  }
}
