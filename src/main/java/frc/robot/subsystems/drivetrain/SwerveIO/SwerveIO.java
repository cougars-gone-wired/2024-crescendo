// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.SwerveIO;

import org.littletonrobotics.junction.AutoLog;

import com.swervedrivespecialties.swervelib.SwerveModule;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import frc.robot.Constants;

/**
 * climber subsystem hardware interface.
 */
public abstract class SwerveIO {
  protected SwerveModule frontLeftModule;
  protected SwerveModule frontRightModule;
  protected SwerveModule backLeftModule;
  protected SwerveModule backRightModule;

  /**
   * Contains all of the input data received from hardware.
   */
  @AutoLog
  public static class SwerveIOInputs {
    public double frontLeftDistanceMeters = 0;
    public Rotation2d frontLeftAngle = new Rotation2d(0);
    public double frontLeftVelocityMPS = 0;
    public double frontRightDistanceMeters = 0;
    public Rotation2d frontRightAngle = new Rotation2d(0);
    public double frontRightVelocityMPS = 0;
    public double backLeftDistanceMeters = 0;
    public Rotation2d backLeftAngle = new Rotation2d(0);
    public double backLeftVelocityMPS = 0;
    public double backRightDistanceMeters = 0;
    public Rotation2d backRightAngle = new Rotation2d(0);
    public double backRightVelocityMPS = 0;

    public SwerveModulePosition[] getPositions() {
      return new SwerveModulePosition[] { 
        new SwerveModulePosition(frontLeftDistanceMeters, frontLeftAngle),
        new SwerveModulePosition(frontRightDistanceMeters, frontRightAngle),
        new SwerveModulePosition(backLeftDistanceMeters, backLeftAngle),
        new SwerveModulePosition(backRightDistanceMeters, backRightAngle)
      };
    }

    public SwerveModuleState[] getStates() {
      return new SwerveModuleState[] {
        new SwerveModuleState(frontLeftVelocityMPS, frontLeftAngle),
        new SwerveModuleState(frontRightVelocityMPS, frontRightAngle),
        new SwerveModuleState(backLeftVelocityMPS, backLeftAngle),
        new SwerveModuleState(backRightVelocityMPS, backRightAngle),
      };
    }
  }

  /**
   * Updates the set of loggable inputs.
   */
  public void updateInputs(SwerveIOInputs inputs) {
    SwerveModulePosition frontLeftPos = frontLeftModule.getPosition();
    SwerveModulePosition frontRightPos = frontRightModule.getPosition();
    SwerveModulePosition backLeftPos = backLeftModule.getPosition();
    SwerveModulePosition backRightPos = backRightModule.getPosition();
    inputs.frontLeftDistanceMeters = frontLeftPos.distanceMeters;
    inputs.frontLeftAngle = frontLeftPos.angle;
    inputs.frontRightDistanceMeters = frontRightPos.distanceMeters;
    inputs.frontRightAngle = frontRightPos.angle;
    inputs.backLeftDistanceMeters = backLeftPos.distanceMeters;
    inputs.backLeftAngle = backLeftPos.angle;
    inputs.backRightDistanceMeters = backRightPos.distanceMeters;
    inputs.backRightAngle = backRightPos.angle;

    SwerveModuleState frontLeftState = frontLeftModule.getState();
    SwerveModuleState frontRightState = frontRightModule.getState();
    SwerveModuleState backLeftState = backLeftModule.getState();
    SwerveModuleState backRightState = backRightModule.getState();
    inputs.frontLeftVelocityMPS = frontLeftState.speedMetersPerSecond;
    inputs.frontRightVelocityMPS = frontRightState.speedMetersPerSecond;
    inputs.backLeftVelocityMPS = backLeftState.speedMetersPerSecond;
    inputs.backRightVelocityMPS = backRightState.speedMetersPerSecond;
  }

  public void setModules(SwerveModuleState[] states) {
    frontLeftModule.set(
      states[0].speedMetersPerSecond / Constants.MAX_VELOCITY_METERS_PER_SECOND * Constants.MAX_VOLTAGE,
      states[0].angle.getRadians()
    );
    frontRightModule.set(
      states[1].speedMetersPerSecond / Constants.MAX_VELOCITY_METERS_PER_SECOND * Constants.MAX_VOLTAGE,
      states[1].angle.getRadians()
    );
    backLeftModule.set(
      states[2].speedMetersPerSecond / Constants.MAX_VELOCITY_METERS_PER_SECOND * Constants.MAX_VOLTAGE,
      states[2].angle.getRadians()
    );
    backRightModule.set(
      states[3].speedMetersPerSecond / Constants.MAX_VELOCITY_METERS_PER_SECOND * Constants.MAX_VOLTAGE,
      states[3].angle.getRadians()
    );
  }
}
