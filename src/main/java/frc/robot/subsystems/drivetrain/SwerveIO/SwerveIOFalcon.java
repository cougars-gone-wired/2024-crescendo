// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain.SwerveIO;

import frc.robot.Constants;

import com.ctre.phoenix6.hardware.TalonFX;
import com.swervedrivespecialties.swervelib.MkSwerveModuleBuilder;
import com.swervedrivespecialties.swervelib.SdsModuleConfigurations;
import com.swervedrivespecialties.swervelib.MotorType;


public class SwerveIOFalcon extends SwerveIO {
  public SwerveIOFalcon(Constants constants) {
    frontLeftModule = new MkSwerveModuleBuilder()
        .withGearRatio(SdsModuleConfigurations.MK4I_L3)
        .withDriveMotor(MotorType.FALCON,
            constants.Drivetrain.FRONT_LEFT_MODULE.DRIVE_MOTOR)
        .withSteerMotor(MotorType.FALCON,
            constants.Drivetrain.FRONT_LEFT_MODULE.STEER_MOTOR)
        .withSteerEncoderPort(constants.Drivetrain.FRONT_LEFT_MODULE.STEER_ENCODER)
        .withSteerOffset(constants.Drivetrain.FRONT_LEFT_MODULE.STEER_OFFSET)
        .build();

    frontRightModule = new MkSwerveModuleBuilder()
        .withGearRatio(SdsModuleConfigurations.MK4I_L3)
        .withDriveMotor(MotorType.FALCON,
            constants.Drivetrain.FRONT_RIGHT_MODULE.DRIVE_MOTOR)
        .withSteerMotor(MotorType.FALCON,
            constants.Drivetrain.FRONT_RIGHT_MODULE.STEER_MOTOR)
        .withSteerEncoderPort(constants.Drivetrain.FRONT_RIGHT_MODULE.STEER_ENCODER)
        .withSteerOffset(constants.Drivetrain.FRONT_RIGHT_MODULE.STEER_OFFSET)
        .build();

    backLeftModule = new MkSwerveModuleBuilder()
        .withGearRatio(SdsModuleConfigurations.MK4I_L3)
        .withDriveMotor(MotorType.FALCON,
            constants.Drivetrain.BACK_LEFT_MODULE.DRIVE_MOTOR)
        .withSteerMotor(MotorType.FALCON,
            constants.Drivetrain.BACK_LEFT_MODULE.STEER_MOTOR)
        .withSteerEncoderPort(constants.Drivetrain.BACK_LEFT_MODULE.STEER_ENCODER)
        .withSteerOffset(constants.Drivetrain.BACK_LEFT_MODULE.STEER_OFFSET)
        .build();

    backRightModule = new MkSwerveModuleBuilder()
        .withGearRatio(SdsModuleConfigurations.MK4I_L3)
        .withDriveMotor(MotorType.FALCON,
            constants.Drivetrain.BACK_RIGHT_MODULE.DRIVE_MOTOR)
        .withSteerMotor(MotorType.FALCON,
            constants.Drivetrain.BACK_RIGHT_MODULE.STEER_MOTOR)
        .withSteerEncoderPort(constants.Drivetrain.BACK_RIGHT_MODULE.STEER_ENCODER)
        .withSteerOffset(constants.Drivetrain.BACK_RIGHT_MODULE.STEER_OFFSET)
        .build();
    

    // ((TalonFX) frontRightModule.getDriveMotor()).optimizeBusUtilization();
    // ((TalonFX) frontRightModule.getSteerMotor()).optimizeBusUtilization();
    // ((TalonFX) frontLeftModule.getDriveMotor()).optimizeBusUtilization();
    // ((TalonFX) frontLeftModule.getSteerMotor()).optimizeBusUtilization();
    // ((TalonFX) backRightModule.getDriveMotor()).optimizeBusUtilization();
    // ((TalonFX) backRightModule.getSteerMotor()).optimizeBusUtilization();
    // ((TalonFX) backLeftModule.getDriveMotor()).optimizeBusUtilization();
    // ((TalonFX) backLeftModule.getSteerMotor()).optimizeBusUtilization();
  }
}
