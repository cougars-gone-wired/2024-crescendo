// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain;

import frc.robot.Constants;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIO;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIOADIS16470;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIOADXRS450;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIONavx2;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIONavxAd16470;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIONavxAd450;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIOPigeon2;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIOSim;
import frc.robot.subsystems.drivetrain.LimelightIO.LimelightIO;
import frc.robot.subsystems.drivetrain.LimelightIO.LimelightIOReal;
import frc.robot.subsystems.drivetrain.SwerveIO.SwerveIO;
import frc.robot.subsystems.drivetrain.SwerveIO.SwerveIOFalcon;
import frc.robot.subsystems.drivetrain.SwerveIO.SwerveIOSim;

public class DrivetrainSubstystemFactory {
    public static DrivetrainSubsystem build(Constants constants) {
        return new DrivetrainSubsystem(
            SwerveIOFactory.build(constants),
            GyroIOFactory.build(constants),
            VisionIOFactory.build(constants));
    }

    public class SwerveIOFactory {
        public static SwerveIO build(Constants constants) {
            if (constants.Drivetrain.ENABLED) {
                return new SwerveIOFalcon(constants);
            } else {
                System.out.println("Using Simulated Drivetrain...");
                return new SwerveIOSim();
            }
        }
    }

    public class GyroIOFactory {
        public static GyroIO build(Constants constants) {
            switch(constants.Gyro) {
                case PIGEON2:
                    return new GyroIOPigeon2();
                case NAVX:
                    return new GyroIONavx2();
                case ADIS16470:
                    return new GyroIOADIS16470();
                case ADXRS450:
                    return new GyroIOADXRS450();
                case NAVX_ADX450:
                    return new GyroIONavxAd450();
                case NAVX_ADIS16470:
                    return new GyroIONavxAd16470();
                default:
                    return new GyroIOSim();
            }
        }
    }

    public class VisionIOFactory {
        public static LimelightIO build(Constants constants) {
            return new LimelightIOReal();
        }
    }
}