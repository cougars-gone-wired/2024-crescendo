// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.drivetrain;

import frc.robot.Constants;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIO;
import frc.robot.subsystems.drivetrain.GyroIO.GyroIOInputsAutoLogged;

import frc.robot.subsystems.drivetrain.SwerveIO.SwerveIO;
import frc.robot.subsystems.drivetrain.SwerveIO.SwerveIOInputsAutoLogged;

import frc.robot.subsystems.drivetrain.LimelightIO.Limelight;
import frc.robot.subsystems.drivetrain.LimelightIO.LimelightIO;
import frc.robot.subsystems.drivetrain.LimelightIO.LimelightIOInputsAutoLogged;

import frc.robot.RobotContainer;
import frc.robot.Constants.DrivetrianConstants.RotationOverride;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.numbers.*;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.DoubleArrayPublisher;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;

import java.nio.file.Path;
import java.util.Optional;

import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.controllers.PPHolonomicDriveController;
import com.pathplanner.lib.path.PathPlannerPath;
import com.pathplanner.lib.path.PathPoint;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.PathPlannerLogging;
import com.pathplanner.lib.util.ReplanningConfig;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.wpilibj.Timer;

public class DrivetrainSubsystem extends SubsystemBase {
    // The locations for the modules must be relative to the center of the robot. 
    // Positive x values represent moving toward the front of the robot.
    // Positive y values represent moving toward the left of the robot.
    public final SwerveDriveKinematics m_kinematics = new SwerveDriveKinematics(
        // front left (+/+)
        new Translation2d(
            Constants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            Constants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // front right (+/-)
        new Translation2d(
            Constants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            -Constants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // back left (-/+)
        new Translation2d(
            -Constants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            Constants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        ),
        // back right (-/-)
        new Translation2d(
            -Constants.DRIVETRAIN_WHEELBASE_METERS / 2.0,
            -Constants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0
        )
    );

    private SwerveDrivePoseEstimator m_poseEstimator = null;

    private ChassisSpeeds m_chassisSpeeds = new ChassisSpeeds(0.0, 0.0, 0.0);
    private final Limelight m_limelight;

    // two fields for the purpose of visualizing drift between tag updated pose and odometry pose
    private final Field2d m_fieldEstimator = new Field2d();
    private final Field2d m_fieldOdometry = new Field2d();
    
    protected final ShuffleboardTab m_shuffleboardTab = Shuffleboard.getTab("Drivetrain");

    private SwerveDriveOdometry m_odometry;
    private final SwerveIO swerveIO;
    private final SwerveIOInputsAutoLogged swerveInputs = new SwerveIOInputsAutoLogged();
    private final GyroIO gyroIO;
    private final GyroIOInputsAutoLogged gyroInputs = new GyroIOInputsAutoLogged();
    private final LimelightIO limelightIO;
    private final LimelightIOInputsAutoLogged limelightInputs = new LimelightIOInputsAutoLogged();
    private static RotationOverride m_currentRotationOverride = RotationOverride.NONE; 
    private static Rotation2d angle = Rotation2d.fromDegrees(0);

    // These are the motion constraints for the rotation controller --> should probably be moved to Constants.DrivetrainConstants
    private final TrapezoidProfile.Constraints m_rotationConstraints = new TrapezoidProfile.Constraints(Units.degreesToRadians(520), Units.degreesToRadians(720));

    // Setup a profiled pid controller for smooth drivetrain rotation control and making the robot face a desired location / direction
    // Takes in a kP, kI, kD, and TrapezoidProfile.Constraints object for motion constraints
    private final ProfiledPIDController m_rotationController = new ProfiledPIDController(5.0, 0, 0, m_rotationConstraints);

    /**
     * Constructor.
     */
    public DrivetrainSubsystem(SwerveIO swerveIO, GyroIO gyroIO, LimelightIO limelightIO) {
        m_fieldOdometry.getObject("Poi").setPose(new Pose2d(0.28, 5.6, new Rotation2d()));
        this.swerveIO = swerveIO;
        this.swerveIO.updateInputs(swerveInputs);
        this.gyroIO = gyroIO;
        this.gyroIO.updateInputs(gyroInputs, 0);
        this.limelightIO = limelightIO;

        m_limelight = new Limelight(this.limelightIO, limelightInputs);

        Rotation2d yaw = gyroInputs.yawDegreesCCW;
        SwerveModulePosition[] positions = swerveInputs.getPositions();
        PPHolonomicDriveController.setRotationTargetOverride(() -> getRotationTargetOverride());


        m_odometry = new SwerveDriveOdometry(
            m_kinematics,
            yaw,
            positions
        );

        m_poseEstimator = new SwerveDrivePoseEstimator(
            m_kinematics, 
            yaw, 
            positions,
            getPose()
        );

        m_shuffleboardTab.addNumber("Gyro Yaw Degrees", () -> gyroInputs.yawDegreesCCW.getDegrees());
        m_shuffleboardTab.addNumber("Rotation Degrees", () -> getRotation().getDegrees());
        m_shuffleboardTab.addNumber("Pose X (B<->F)", () -> m_odometry.getPoseMeters().getX());
        m_shuffleboardTab.addNumber("Pose Y (R<->L)", () -> m_odometry.getPoseMeters().getY());
        
        SmartDashboard.putData("Field Estimator", m_fieldEstimator);
        SmartDashboard.putData("Field Odometry", m_fieldOdometry);

        // Configure AutoBuilder for pathplanner
        AutoBuilder.configureHolonomic(
            this::getEstimatedPose,
            this::resetPose,
            // () -> m_kinematics.toChassisSpeeds(getModuleStates()),
            this::getChassisSpeeds,
            this::driveBotRelative,
            new HolonomicPathFollowerConfig(
                new PIDConstants(5.0, 0.0, 0.0), // translation pid //FIXME
                new PIDConstants(5.0, 0.0, 0.0), // rotation pid //FIXME
                Constants.MAX_VELOCITY_METERS_PER_SECOND,
                0.45, // drivebase radius //FIXME --> read pram description to set
                new ReplanningConfig()
            ),
            () -> {
                var alliance = DriverStation.getAlliance();
                if (alliance.isPresent()) {
                    return alliance.get() == DriverStation.Alliance.Red;
                }

                return false; // default is blue
            },
            this
        );

        PathPlannerLogging.setLogActivePathCallback(
            (activePath) -> {
                Logger.recordOutput(
                    "Path Following/Trajectory", activePath.toArray(new Pose2d[activePath.size()]));
            });
        PathPlannerLogging.setLogTargetPoseCallback(
            (targetPose) -> {
                Logger.recordOutput("Path Following/TrajectorySetpoint", targetPose);
            }
        );

       
    }
    
    /*
     * Method to override target rotation for Beyblade auto.
     */


    public static Optional<Rotation2d> getRotationTargetOverride() {
        switch(m_currentRotationOverride) {
           
            case SPIN: 
                angle = angle.plus(Rotation2d.fromDegrees((DriverStation.getAlliance().get() == Alliance.Red ? 1 : -1)* 186/50.0));
                return Optional.of(angle); 

            case NONE: 
                angle = Rotation2d.fromDegrees(0);
                return Optional.empty();

            default: return Optional.empty();
           
        }
    }

    public Command setRotationState(RotationOverride rotationState) {
        return new InstantCommand(()-> m_currentRotationOverride = rotationState);
    }

    

    /**
     * Sets the odometry offset to the current gyro angle. 
     * This can be used to set the direction the robot is 
     * currently facing to the new 'forwards' direction (0 degrees).
     */
    public void zeroGyroscope() {
        m_odometry.resetPosition(
            gyroInputs.yawDegreesCCW,
            swerveInputs.getPositions(),
            new Pose2d(m_odometry.getPoseMeters().getTranslation(), Rotation2d.fromDegrees(0.0))
        );
    }

    /**
     * Current odometry rotation in relation to the latest reset.
     * @return {Rotation2d} Odometry rotation.  
     */
    public Rotation2d getRotation() {
        return m_odometry.getPoseMeters().getRotation();
    }

    public Pose2d getPose() {
        return m_odometry.getPoseMeters();
    }
        
    /**
     * Used by Path Planner to read current estimated pose.
     * @returns {Pose2d} Current robot position as reported by odometry.
     */
    public Pose2d getEstimatedPose() {
        return m_poseEstimator.getEstimatedPosition();
    }

    @AutoLogOutput
    public double getDistanceToSpeaker(Optional<DriverStation.Alliance> alliance) {
        Pose2d temp;
        
        if (alliance.get() == DriverStation.Alliance.Red) {
            temp = Constants.StagePoseConstants.lutRedSpeakerPose;
        } else {
            temp =  Constants.StagePoseConstants.lutBlueSpeakerPose;
        }

        double distance = Math.sqrt(Math.pow(temp.getX() - getEstimatedPose().getX(), 2) + Math.pow(temp.getY() - getEstimatedPose().getY(), 2));
        Logger.recordOutput("Distance from speaker", distance);
        return distance;
    }

    /**
     * Used by Path Planner to set robot starting pose.
     * @param {Pose2d} Position to set.
     */
    public void resetPose(Pose2d pose) {
        SwerveModulePosition[] positions = swerveInputs.getPositions();
        Rotation2d yaw = gyroInputs.yawDegreesCCW;

        m_poseEstimator.resetPosition(
            yaw,
            positions,
            pose);

        // if red, reset odometry by PI
        // var isRed = DriverStation.getAlliance().get() == DriverStation.Alliance.Red;
        // Pose2d oPose = new Pose2d(pose.getX(), pose.getY(), isRed ? pose.getRotation().rotateBy(new Rotation2d(Math.PI)) : pose.getRotation());
        m_odometry.resetPosition(
            yaw,
            positions,
            pose);
    }

    /**
     * Used by Path Planner to get the robot's chassis speed
     * 
     */
    public ChassisSpeeds getChassisSpeeds() {
        getModuleStates();
        return m_chassisSpeeds;
    }

    @AutoLogOutput(key = "SwerveModuleStates/measured")
    public SwerveModuleState[] getModuleStates() {
        return swerveInputs.getStates();
    }

    /**
     * Used by Path Planner to set each swerve module's speed and angle.
     * @param {SwerveModuleState[]} states Array of modue states to set.
     */
    public void setModuleStates(SwerveModuleState[] states) {
        m_chassisSpeeds = m_kinematics.toChassisSpeeds(states);
    }

    public void driveFieldRelative(ChassisSpeeds chassisSpeeds) {
        boolean isFlipped = DriverStation.getAlliance().isPresent() && DriverStation.getAlliance().get() == Alliance.Red;
        
        m_chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(chassisSpeeds, isFlipped ? getRotation().plus(new Rotation2d(Math.PI)) : getRotation());
    }

    /**
     * Called to set module speeds to set on the next periodic call.
     * @param {ChassisSpeeds} chassisSpeeds 
     */
    public void driveBotRelative(ChassisSpeeds chassisSpeeds) {
        m_chassisSpeeds = chassisSpeeds;
    }

    public void resetRotationController() {
        m_rotationController.reset(this.getEstimatedPose().getRotation().getRadians());
    }

    /**
     * Used to determine if the drivetrain is facing the desired rotation
     * @return true if the robot is within tolerance of facing the desired rotation
     */
    public boolean isAtDesiredRotation() {
        return m_rotationController.atGoal();
    }


    public double calculateRotationFeedback(TrapezoidProfile.State targetState) {
        // get the current rotation of the robot from the pose estimator (in radians)
        double currRot = this.getEstimatedPose().getRotation().getRadians();
        
        // The purpose of the following operation is to ensure that the robot always travels the shortest distance when rotating
        // and that the way to the shortest distance will never have to cross the angle wrap point (the point when -pi/-180 becomes pi/180)

        // this converts the current rotation into a measurement relative to the target rotation
        // currRot - targetState.position gets the relative angle between the current rotation and the target rotation
        // MathUtil.angleModulus(...) wraps the angle such that it is always between (-pi, pi]
        currRot = MathUtil.angleModulus(currRot - targetState.position);
        targetState.position = 0;
        return m_rotationController.calculate(currRot, targetState, m_rotationConstraints);
    }
    
    public boolean withinTrustedVelocity() {
        return (Math.abs(m_chassisSpeeds.vxMetersPerSecond) < Constants.DrivetrianConstants.TRUSTED_LINEAR_VELOCITY) && (Math.abs(m_chassisSpeeds.vyMetersPerSecond) < Constants.DrivetrianConstants.TRUSTED_LINEAR_VELOCITY) && (Math.abs(m_chassisSpeeds.omegaRadiansPerSecond) < Constants.DrivetrianConstants.TRUSTED_ANGULAR_VELOCITY);
    }

    /**
     * Subsystem periodic function.
     * 1. Update odometry to current module positions.
     * 2. Update kinematics to chassis speeds set by the drive() function
     */
    @Override
    public void periodic() {
        swerveIO.updateInputs(swerveInputs);
        Logger.processInputs("Inputs/Swerve", swerveInputs);
        gyroIO.updateInputs(gyroInputs, m_chassisSpeeds.omegaRadiansPerSecond);
        Logger.processInputs("Inputs/Gyro", gyroInputs);
        limelightIO.updateInputs(limelightInputs);
        Logger.processInputs("Inputs/Limelight", limelightInputs);

        Rotation2d currentRotation = gyroInputs.yawDegreesCCW;
        Pose2d CurrentPose = getPose();
        Pose2d estimatedPose = getEstimatedPose();

        SmartDashboard.putNumber("Radians per second: ", m_chassisSpeeds.omegaRadiansPerSecond);
        SmartDashboard.putNumber("Gyro yaw degrees: ", currentRotation.getDegrees());
        SmartDashboard.putNumber("Odometry X", CurrentPose.getX());
        SmartDashboard.putNumber("Odometry Y", CurrentPose.getY());
        SmartDashboard.putNumber("Odometry Rot: ", CurrentPose.getRotation().getDegrees());
        SmartDashboard.putNumber("Estimator X", estimatedPose.getX());
        SmartDashboard.putNumber("Estimator Y", estimatedPose.getY());
        SmartDashboard.putNumber("Estimator Rot", estimatedPose.getRotation().getDegrees());
        SmartDashboard.putBoolean("validTarget", m_limelight.validTarget());
        
        Logger.recordOutput("Odometry Pose", CurrentPose);
        Logger.recordOutput("Estimated Pose", estimatedPose);
        Logger.recordOutput("Chassis Speeds", m_chassisSpeeds);

        m_fieldOdometry.setRobotPose(CurrentPose);
        
        SwerveModulePosition[] swerveModulePositions = swerveInputs.getPositions();
        Logger.recordOutput("Swerve Module Positions", swerveModulePositions);
        
        m_odometry.update(
            currentRotation,
            swerveModulePositions
        );
        
        // Updates pose estimator with a vision target if there is a target in sight using the latency from the limelight
        if(m_limelight.validTarget() && withinTrustedVelocity()) {

            Pose2d limelightPose = m_limelight.getPose2d();
            double latency = Timer.getFPGATimestamp() - m_limelight.getLatency();
            Matrix<N3,N1> stDeviations = m_limelight.calculateTrust();

            m_poseEstimator.addVisionMeasurement(
                limelightPose, 
                latency,
                stDeviations
            );

            Logger.recordOutput("Limelight Pose2d", m_limelight.getPose2d());
            var std = new double[3];
            for(int i = 0; i < 3; i++) {
                std[i] = stDeviations.get(i, 0);
            }
            Logger.recordOutput("Limelight Standard Deviations", std);
        } 
        m_poseEstimator.update(
            currentRotation,
            swerveModulePositions
        );

        SwerveModuleState[] states = m_kinematics.toSwerveModuleStates(m_chassisSpeeds);
        SwerveDriveKinematics.desaturateWheelSpeeds(states, Constants.MAX_VELOCITY_METERS_PER_SECOND);
        swerveIO.setModules(states);
        Logger.recordOutput("SwerveModuleStates/target", states);
    }
}
