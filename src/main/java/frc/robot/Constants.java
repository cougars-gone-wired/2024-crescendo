// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.util.GeometryUtil;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;

import com.swervedrivespecialties.swervelib.SdsModuleConfigurations;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose.
 */
public final class Constants {

  public static final double DRIVETRAIN_TRACKWIDTH_METERS = 0.582;
  public static final double DRIVETRAIN_WHEELBASE_METERS = 0.630;
  public GyroType Gyro = GyroType.SIM;
  public DrivetrianConstants Drivetrain = new DrivetrianConstants();
  public ElevatorConstants Elevator = new ElevatorConstants();
  public StagePoseConstants stage = new StagePoseConstants();

  

  public static final PathConstraints Constraints = new PathConstraints(3.0, 4.0, Units.degreesToRadians(540), Units.degreesToRadians(720));

  /**
   * The maximum voltage that will be delivered to the drive motors.
   * <p>
   * This can be reduced to cap the robot's maximum speed. Typically, this is
   * useful during initial testing of the robot.
   */
  public static final double MAX_VOLTAGE = 12.0;

  // The formula for calculating the theoretical maximum velocity is:
  // <Motor free speed RPM> / 60 * <Drive reduction> * <Wheel diameter meters> *
  // pi
  // By default this value is setup for a Mk3 standard module using Falcon500s to
  // drive.
  // An example of this constant for a Mk4 L2 module with NEOs to drive is:
  // 5880.0 / 60.0 / SdsModuleConfigurations.MK4_L2.getDriveReduction() *
  // SdsModuleConfigurations.MK4_L2.getWheelDiameter() * Math.PI

  /**
   * The maximum velocity of the robot in meters per second.
   * <p>
   * This is a measure of how fast the robot should be able to drive in a straight
   * line.
   */
  public static final double MAX_VELOCITY_METERS_PER_SECOND = 6000.0 / 60.0 *
          SdsModuleConfigurations.MK4I_L3.getDriveReduction() *
          SdsModuleConfigurations.MK4I_L3.getWheelDiameter() * Math.PI;
  /**
   * The maximum angular velocity of the robot in radians per second.
   * <p>
   * This is a measure of how fast the robot can rotate in place.
   */
  // Here we calculate the theoretical maximum angular velocity. You can also
  // replace this with a measured amount.
  public static final double MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND = MAX_VELOCITY_METERS_PER_SECOND /
          Math.hypot(Constants.DRIVETRAIN_TRACKWIDTH_METERS / 2.0,
                  Constants.DRIVETRAIN_WHEELBASE_METERS / 2.0);


  /**
   * Constructor
   * 
   * @param RobotName Construct constants for this robot.
   */
  public Constants(String robotName) {

    
    
    if (robotName.equals("Fred")) {
      Gyro = GyroType.PIGEON2;
      Drivetrain = new DrivetrianConstants(
        new SwerveModuleConstants(21, 3, 12, Math.toDegrees(0.391)), // Patrck - FL
        new SwerveModuleConstants(24, 6, 10, Math.toDegrees(2.787)), // Bernard - FR
        new SwerveModuleConstants(23, 8, 11, Math.toDegrees(1.6)), //John - BL
        new SwerveModuleConstants(22, 2, 9, Math.toDegrees(5.611)) // Mister-BR
      );
      Elevator = new ElevatorConstants(45, 46);

    } else if (robotName.equals("Test Bench")) {
      Gyro = GyroType.NAVX;
    } else if (robotName.equals("GrabNGo")) {
      // Gyro = GyroType.ADXRS450;
    }
  }

  public enum GyroType {
    SIM,
    PIGEON2,
    NAVX,
    ADIS16470,
    ADXRS450,
    NAVX_ADX450,
    NAVX_ADIS16470
  }

  /**
   * Holds drivetrain constants.
   */
  public final class DrivetrianConstants {
    public final Boolean ENABLED;
    public final SwerveModuleConstants FRONT_LEFT_MODULE;
    public final SwerveModuleConstants FRONT_RIGHT_MODULE;
    public final SwerveModuleConstants BACK_LEFT_MODULE;
    public final SwerveModuleConstants BACK_RIGHT_MODULE;
    public static final double TRUSTED_LINEAR_VELOCITY = 2.3; // FIXME
    public static final double TRUSTED_ANGULAR_VELOCITY = 2.1 * Math.PI; // FIXME
    public enum RotationOverride {
      NONE,
      SPIN
    };

    /**
     * Default contructor to set disabled
     */
    DrivetrianConstants () {
        ENABLED = false;
        FRONT_LEFT_MODULE = new SwerveModuleConstants();
        FRONT_RIGHT_MODULE = new SwerveModuleConstants();
        BACK_LEFT_MODULE = new SwerveModuleConstants();
        BACK_RIGHT_MODULE = new SwerveModuleConstants();
    };

    /**
     * Constructor.
     * 
     * @param frontLeft The constants for the front left swerve module.
     * @param frontRight The constants for the front right swerve module.
     * @param backLeft The constants for the back left swerve module.
     * @param backRight The constants for the back right swerve module.
     */
    DrivetrianConstants (
        SwerveModuleConstants frontLeft,
        SwerveModuleConstants frontRight,
        SwerveModuleConstants backLeft,
        SwerveModuleConstants backRight
    ) {
      ENABLED = true;
      FRONT_LEFT_MODULE = frontLeft;
      FRONT_RIGHT_MODULE = frontRight;
      BACK_LEFT_MODULE = backLeft;
      BACK_RIGHT_MODULE = backRight;
    }
  }

  /**
   * Constantants for a swerve module.
   */
  public final class SwerveModuleConstants {
    public final int DRIVE_MOTOR;
    public final int STEER_MOTOR;
    public final int STEER_ENCODER;
    public final double STEER_OFFSET;

    /**
     * Default constuctor.
     */
    public SwerveModuleConstants() {
      DRIVE_MOTOR = -1;
      STEER_MOTOR = -1;
      STEER_ENCODER = -1;
      STEER_OFFSET = 0;        
    }

    /**
     * Constructor.
     * 
     * @param driveMotor CAN address od module drive motor.
     * @param steerMotor CAN address od module steer motor.
     * @param steerEncoder CAN address od module steer encoder.
     * @param steerOffsetDegrees Calibrated angle offset in degrees.
     */
    public SwerveModuleConstants(int driveMotor, int steerMotor, int steerEncoder, double steerOffsetDegrees) {
      DRIVE_MOTOR = driveMotor;
      STEER_MOTOR = steerMotor;
      STEER_ENCODER = steerEncoder;
      STEER_OFFSET = -Math.toRadians(steerOffsetDegrees);        
    }
  }

    public final class ElevatorConstants {
      public final boolean ENABLED;
      public final int LEFT_MOTOR_ID;
      public final int RIGHT_MOTOR_ID;
      // public final int LEFT_ENCODER;
      // public final int RIGHT_ENCODER;
      public static final double ELEVATOR_SPEED = 0.2;

    
      public ElevatorConstants() {
        ENABLED = false;
        LEFT_MOTOR_ID = -1;
        RIGHT_MOTOR_ID = -1;
        // LEFT_ENCODER = -1;
        // RIGHT_ENCODER = -1;
      }

      public ElevatorConstants(int leftMotorID, int rightMotorID) {
        ENABLED = true;
        LEFT_MOTOR_ID = leftMotorID;
        RIGHT_MOTOR_ID = rightMotorID;
     }
  }




  public static final class StagePoseConstants {
    public static final Pose2d lutBlueSpeakerPose = new Pose2d(0.46,5.545,Rotation2d.fromDegrees(0.00));
    public static final Pose2d targBlueSpeakerPose = new Pose2d(0.26,5.545,Rotation2d.fromDegrees(0.00));
    // The tag location for Blue Amp is
    public static final Pose2d blueAmpPose = new Pose2d(1.84, 7.97,Rotation2d.fromDegrees(0.00));
    public static final Pose2d lutRedSpeakerPose = GeometryUtil.flipFieldPose(lutBlueSpeakerPose);
    public static final Pose2d targRedSpeakerPose = GeometryUtil.flipFieldPose(targBlueSpeakerPose);
    
    // The tag location for Red Amp is
    public static final Pose2d redAmpPose = new Pose2d(14.67, 7.97,Rotation2d.fromDegrees(0.00));
    public static final Pose2d redStagePose = new Pose2d(11.7, 4, Rotation2d.fromDegrees(0));
    public static final Pose2d blueStagePose = new Pose2d(4.8, 4, Rotation2d.fromDegrees(0));
  }

}
