// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import frc.robot.Constants.ElevatorConstants;
import frc.robot.commands.elevatorCommands.elevatorDown;
import frc.robot.commands.elevatorCommands.elevatorUp;
import frc.robot.commands.driveCommands.DefaultDriveCommand;
import frc.robot.commands.driveCommands.ZeroCommand;
import frc.robot.commands.driveCommands.LookAtPoseCommand;
import frc.robot.Constants.DrivetrianConstants.RotationOverride;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;

import org.littletonrobotics.junction.Logger;
import org.littletonrobotics.junction.networktables.LoggedDashboardChooser;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;

import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.subsystems.drivetrain.DrivetrainSubsystem;
import frc.robot.subsystems.elevator.ElevatorSubsystem;
import frc.robot.subsystems.elevator.ElevatorSubsystemFactory;
import frc.robot.subsystems.drivetrain.DrivetrainSubstystemFactory;
import frc.robot.subsystems.elevator.ElevatorSubsystem;
import frc.robot.subsystems.elevator.ElevatorSubsystemFactory;
import frc.robot.commands.VariableAuto;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot (including
 * subsystems, commands, and trigger mappings) should be declared here.
 */
public class RobotContainer {
  // Create driver controllers
  private final CommandXboxController m_driverController = new CommandXboxController(0);
  private final CommandXboxController m_manipulatorController = new CommandXboxController(1);

  // The robot's subsystems and commands are defined here...
  private final DrivetrainSubsystem m_drivetrainSubsystem;
  private final ElevatorSubsystem m_ElevatorSubsystem;

  private LoggedDashboardChooser<Command> m_autoChooser;
  private VariableAuto m_variableAuto;


  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer(Constants constants) {
    // create subsystems
    m_drivetrainSubsystem = DrivetrainSubstystemFactory.build(constants);
    m_ElevatorSubsystem = ElevatorSubsystemFactory.build(constants);

    Logger.recordOutput("Constants/LUT Blue Speaker Pose", Constants.StagePoseConstants.lutBlueSpeakerPose);
    Logger.recordOutput("Constants/LUT Red Speaker Pose", Constants.StagePoseConstants.lutRedSpeakerPose);
    Logger.recordOutput("Constants/Target Blue Speaker Pose", Constants.StagePoseConstants.targBlueSpeakerPose);
    Logger.recordOutput("Constants/Target Red Speaker Pose", Constants.StagePoseConstants.targRedSpeakerPose);

    // Configure the trigger bindings
    configureNamedCommands();
    configureBindings();
    configureAutos();
  }

  public void configureAutos() {
    m_variableAuto = new VariableAuto();
    m_autoChooser = new LoggedDashboardChooser<Command>("Auto Selector", AutoBuilder.buildAutoChooser("Kellen auto"));
    m_autoChooser.addOption("OMAHA", m_variableAuto.getCommandGroup());
  }

  public static final double JoystickCancelThreshold = 0.1;

  public Trigger MoveLeftJoystick() {
    return 
      m_driverController.axisLessThan(0, -JoystickCancelThreshold).or(
      m_driverController.axisGreaterThan(0, JoystickCancelThreshold).or(
      m_driverController.axisLessThan(1, -JoystickCancelThreshold).or(
      m_driverController.axisGreaterThan(1, JoystickCancelThreshold))));
  }

  public Trigger MoveRightJoystick() {
    return  
      m_driverController.axisLessThan(4, -JoystickCancelThreshold).or(
      m_driverController.axisGreaterThan(4, JoystickCancelThreshold).or(
      m_driverController.axisLessThan(5, -JoystickCancelThreshold).or(
      m_driverController.axisGreaterThan(5, JoystickCancelThreshold))));
  }

  /**
   * Use this method to define your trigger->command mappings. Triggers can be
   * created via the
   * {@link Trigger#Trigger(java.util.function.BooleanSupplier)} constructor with
   * an arbitrary
   * predicate, or via the named factories in {@link
   * edu.wpi.first.wpilibj2.command.button.CommandGenericHID}'s subclasses for
   * {@link
   * CommandXboxController
   * Xbox}/{@link edu.wpi.first.wpilibj2.command.button.CommandPS4Controller
   * PS4} controllers or
   * {@link edu.wpi.first.wpilibj2.command.button.CommandJoystick Flight
   * joysticks}.
   */
  private void configureBindings() {
    // Default commands for subsystems
    m_drivetrainSubsystem.setDefaultCommand(
        new DefaultDriveCommand(
            m_drivetrainSubsystem,
            () -> -modifyAxis(m_driverController.getLeftY()),
            () -> -modifyAxis(m_driverController.getLeftX()),
            () -> -modifyAxis(m_driverController.getRightX()),
            m_driverController.getHID()::getRightBumper));

    // Driver Button Bindings
    m_driverController.back().onTrue(new ZeroCommand(m_drivetrainSubsystem));
    m_driverController.x().toggleOnTrue(
      new LookAtPoseCommand(
        () -> getSpeakerPose(), 
        () -> -modifyAxis(m_driverController.getLeftY()), 
        () -> -modifyAxis(m_driverController.getLeftX()),
         m_drivetrainSubsystem).until(MoveRightJoystick())
      );

    m_driverController.leftBumper().toggleOnTrue(
      new LookAtPoseCommand(
        () -> getSpeakerPose(), 
        () -> -modifyAxis(m_driverController.getLeftY()), 
        () -> -modifyAxis(m_driverController.getLeftX()),
         m_drivetrainSubsystem).until(MoveRightJoystick())
      );
      m_manipulatorController.povUp().whileTrue(new elevatorUp(m_ElevatorSubsystem));
      m_manipulatorController.povDown().whileTrue(new elevatorDown(m_ElevatorSubsystem));
  }
    // m_driverController.rightTrigger(0.3).whileTrue(new ExtendCommand(m_climber));
    // m_driverController.leftTrigger(0.3).whileTrue(new RetractClimberCommand(m_climber));

    // m_driverController.a().toggleOnTrue(
    //   new StageLockCommand(m_drivetrainSubsystem, 
    //   () -> -modifyAxis(m_driverController.getLeftY()), 
    //   () -> -modifyAxis(m_driverController.getLeftX()))
    // );

      
    // Manipulator Button Bindings

    // Button for stash shot over stage
    // (also doubles as a subwoofer shot if needed)

    // m_manipulatorController.x().toggleOnTrue(new AutoShootCommand(
    //   m_shooterSubsystem, 
    //   () -> m_shooterConstants.getLeftShootingVel(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance())),
    //   () -> m_shooterConstants.getRightShootingVel(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance())),
    //   m_wristSubsystem,
    //   () -> m_shooterConstants.getShootingAngle(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance())),
    //   m_feederSubsystem,
    //   m_armSubsystem));

    // m_manipulatorController.a().toggleOnTrue(new ShooterCommand(m_shooterSubsystem, () -> m_shooterConstants.getLeftShootingVel(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance())), () -> m_shooterConstants.getLeftShootingVel(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance()))).alongWith(new WristGoToContinuousPositionCommand(m_wristSubsystem, () -> m_shooterConstants.getShootingAngle(m_drivetrainSubsystem.getDistanceToSpeaker(DriverStation.getAlliance())))));

      

  public void configureNamedCommands() {
    NamedCommands.registerCommand("isSpin", m_drivetrainSubsystem.setRotationState(RotationOverride.SPIN));
    NamedCommands.registerCommand("isNoSpin", m_drivetrainSubsystem.setRotationState(RotationOverride.NONE));
  }
  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An example command will be run in autonomous
    if (m_autoChooser.get().getName().equals("OMAHA")) {
      return m_variableAuto.getCommandGroup();
    }
    return m_autoChooser.get();
  }

  public Pose2d getSpeakerPose() {
    if (DriverStation.getAlliance().isPresent() && DriverStation.getAlliance().get() == DriverStation.Alliance.Red) {
      return Constants.StagePoseConstants.targRedSpeakerPose;
    }
    return Constants.StagePoseConstants.targBlueSpeakerPose;
  }

  public Pose2d getStagePose() {
    if (DriverStation.getAlliance().get() == DriverStation.Alliance.Red) {
       return Constants.StagePoseConstants.redStagePose;
    }
    return Constants.StagePoseConstants.blueStagePose;
  }

  
  /**
   * Modify joystick axis value.
   * 
   * @param value Axis value to modify.
   * @return Modified value.
   */
  private static double modifyAxis(double value) {
    // Deadband
    value = MathUtil.applyDeadband(value, 0.05);

    // Square axis input to give finer control
    value = Math.copySign(value * value, value);
    return value;
  }

  /**
   * Log the Robot components.
   * 
   * @param intakeAngle      Intake angle from floor position in radians.
   * @param wristAngle       Wrist pitch angle from stow position in radians.
   * @param armAngle         Arm raise angle from down position in radians.
   * @param climberExtension Extension distance in meters.
   */
  public void LogRobotComponents(
    Rotation2d intakeAngle,
    Rotation2d wristAngle,
    Rotation2d armAngle
  ) {
    Pose3d intake3d = new Pose3d(-0.33, 0, 0.25, new Rotation3d(0, intakeAngle.getRadians(), 0));

    Pose3d arm3d = new Pose3d(0.33, 0, 0.31, new Rotation3d(0, armAngle.getRadians(), 0));

    // calculate shooter position as a function of arm angle
    double armLength = 0.475;
    double dx = armLength - armLength * Math.cos(armAngle.getRadians());
    double dz = armLength * Math.sin(armAngle.getRadians());

    // offset wrist by 52 degrees
    wristAngle = Rotation2d.fromDegrees(wristAngle.getDegrees() - 52);
    Pose3d shooter3d = new Pose3d(-0.145 + dx, 0, 0.32 + dz, new Rotation3d(0, armAngle.getRadians() - wristAngle.getRadians(), 0));

    // log all robot components
    Logger.recordOutput("Robot 3D Segments", new Pose3d[] {
        intake3d,
        arm3d,
        shooter3d
    });
  }

  public void periodic() {
  //   LogRobotComponents(
  //       new Rotation2d(), //TODO: add actual intake rotation
  //       m_wristSubsystem.getWristAngle(),
  //       m_armSubsystem.getAngle());
  //   SmartDashboard.putBoolean("Compressor Enabled", m_compressorEnabled);
  //   Logger.recordOutput("Compressor Enabled", m_compressorEnabled);
  //   Logger.recordOutput("I think we shot", m_shooterSubsystem.mayHaveShot() && m_feederSubsystem.getSpeed() > 0);
   }
}
