package frc.robot.sim;

import com.swervedrivespecialties.swervelib.AbsoluteEncoder;
import com.swervedrivespecialties.swervelib.SwerveModule;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public class SwerveModuleSim implements SwerveModule {
    final private MotorSim _driveMotor;
    final private MotorSim _steerMotor;
    private double _steerAngle = 0;

    public SwerveModuleSim(double maxVelocity) {
        _driveMotor = new MotorSim(maxVelocity);
        _steerMotor = new MotorSim(2*Math.PI);
    }

    @Override
    public MotorController getDriveMotor() {
        return _driveMotor;
    }

    @Override
    public MotorController getSteerMotor(){
        return _steerMotor;
    }

    @Override
    public AbsoluteEncoder getSteerEncoder(){
        return _steerMotor; 
    }

    /**
     * Get module drive velocity
     * 
     * @return drive velocity in m/s
     */
    @Override
    public double getDriveVelocity(){
        return _driveMotor.getVelocity(); 
    }

    /**
     * Get module drive distance
     * 
     * @return drive distance in meters
     */
    @Override
    public double getDriveDistance(){
        return _driveMotor.getDistance();
    }

    /**
     * Get module steer angle
     * 
     * @return steer angle in radians from [0, 2pi)
     */
    @Override
    public double getSteerAngle(){
        return _steerAngle;
    }

    /**
     * Reset motor or encoder position to the absolute position. May take a little bit.
     */
    @Override
    public void resetToAbsolute(){
    }

    @Override
    public void set(double driveVoltage, double steerAngle){
        _driveMotor.setVoltage(driveVoltage);
        _steerAngle = steerAngle;
    }

    @Override
    public SwerveModulePosition getPosition() {
        _driveMotor.periodic();
        _steerMotor.periodic();
        return new SwerveModulePosition(getDriveDistance(), Rotation2d.fromRadians(getSteerAngle()));
    }
}
