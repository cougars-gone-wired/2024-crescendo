package frc.robot.commands.elevatorCommands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.elevator.ElevatorSubsystem;

public class elevatorUp extends Command {
    private final ElevatorSubsystem m_elevatorSubsystem;

    public elevatorUp(ElevatorSubsystem elevatorSubsystem) {
        m_elevatorSubsystem = elevatorSubsystem;

        addRequirements(elevatorSubsystem);
    }

    @Override
    public void initialize() {
        m_elevatorSubsystem.elevatorUp();
    }

    @Override
    public void end(boolean interrupted) {
            m_elevatorSubsystem.stopElevator();
    }


}
