package frc.robot.commands;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.path.PathConstraints;
import frc.robot.Constants;


import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;

public class GoToPreset {

    private static PathConstraints m_constraints = Constants.Constraints;


    public static Command pathfindingCommand(Pose2d targetPose) {
       return AutoBuilder.pathfindToPose(
        targetPose,  
        m_constraints,
        0.0,
        0.1); 
    }

    public static Command pathfindingCommand(int x, int y, Rotation2d deg) {
        return pathfindingCommand(new Pose2d(x, y, deg)); 
    }

    public static Command [] allthePosesCommands(Pose2d [] poses) {
        Command [] generatedCommands = new Command[poses.length];
        for(int i = 0; i < poses.length; i++ ) {
            generatedCommands[i] = pathfindingCommand(poses[i]);
        }
        return generatedCommands; 
    }

    
}

