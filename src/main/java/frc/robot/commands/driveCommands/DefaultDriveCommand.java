package frc.robot.commands.driveCommands;

import frc.robot.Constants;

import java.util.function.DoubleSupplier;
import java.util.function.BooleanSupplier;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.drivetrain.DrivetrainSubsystem;

public class DefaultDriveCommand extends Command {
    private final DrivetrainSubsystem m_drivetrainSubsystem;
    
    private final DoubleSupplier m_translationXSupplier;
    private final DoubleSupplier m_translationYSupplier;
    private final DoubleSupplier m_rotationSupplier;

    private final BooleanSupplier m_botRelativeSupplier;

    public DefaultDriveCommand(DrivetrainSubsystem drivetrainSubsystem,
                                DoubleSupplier translationXSupplier,
                                DoubleSupplier translationYSupplier,
                                DoubleSupplier rotationSupplier,
                                BooleanSupplier botRelativeSupplier) {
        
        
        m_drivetrainSubsystem = drivetrainSubsystem;

        // multiply by max speeds to translate controller values into chassis speeds
        m_translationXSupplier = () -> translationXSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND;
        m_translationYSupplier = () -> translationYSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND;
        m_rotationSupplier = () -> rotationSupplier.getAsDouble() * Constants.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND;
        m_botRelativeSupplier = botRelativeSupplier;

        addRequirements(m_drivetrainSubsystem);
    }

    @Override
    public void execute() {
        var chassisSpeeds = new ChassisSpeeds(
            m_translationXSupplier.getAsDouble(),
            m_translationYSupplier.getAsDouble(),
            m_rotationSupplier.getAsDouble()
        );

        if(m_botRelativeSupplier.getAsBoolean()) {
            m_drivetrainSubsystem.driveBotRelative(chassisSpeeds);
        } else {
            m_drivetrainSubsystem.driveFieldRelative(chassisSpeeds);
        }
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.driveBotRelative(new ChassisSpeeds(0, 0, 0));
    }
}
