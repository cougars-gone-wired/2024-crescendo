package frc.robot.commands.driveCommands;

import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.drivetrain.DrivetrainSubsystem;

public class LookAtPoseCommand extends Command {
    private final DrivetrainSubsystem m_drivetrainSubsystem;
    private final Supplier<Pose2d> m_poiPoseSupplier;

    private final DoubleSupplier m_translationXSupplier;
    private final DoubleSupplier m_translationYSupplier;
    private final boolean m_reverse;

    public LookAtPoseCommand(Supplier<Pose2d> poiPoseSupplier, DoubleSupplier translationXSupplier, DoubleSupplier translationYSupplier, boolean reverse, DrivetrainSubsystem drivetrainSubsystem) {
        m_drivetrainSubsystem = drivetrainSubsystem;
        m_poiPoseSupplier = poiPoseSupplier;
        m_translationXSupplier = () -> translationXSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND;
        m_translationYSupplier = () -> translationYSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND;
        m_reverse = reverse;
        addRequirements(m_drivetrainSubsystem);
    }

    public LookAtPoseCommand(Pose2d poiPose, DoubleSupplier translationXSupplier, DoubleSupplier translationYSupplier, boolean reverse, DrivetrainSubsystem drivetrainSubsystem) {
        this(() -> poiPose, translationXSupplier, translationYSupplier, reverse, drivetrainSubsystem);
    }

    public LookAtPoseCommand(Pose2d poiPose, DoubleSupplier translationXSupplier, DoubleSupplier translationYSupplier, DrivetrainSubsystem drivetrainSubsystem) {
        this(() -> poiPose, translationXSupplier, translationYSupplier, false, drivetrainSubsystem);
    }

    public LookAtPoseCommand(Supplier<Pose2d> poiPoseSupplier, DoubleSupplier translationXSupplier, DoubleSupplier translationYSupplier, DrivetrainSubsystem drivetrainSubsystem) {
        this(poiPoseSupplier, translationXSupplier, translationYSupplier, false, drivetrainSubsystem);
    }

    @Override
    public void execute() {
        Pose2d poiPose = m_poiPoseSupplier.get();
        Pose2d currPose = m_drivetrainSubsystem.getEstimatedPose();
        double dy = m_reverse ? currPose.getY()-poiPose.getY() : poiPose.getY()-currPose.getY();
        double dx = m_reverse ? currPose.getX()-poiPose.getX() : poiPose.getX()-currPose.getX();
        double desiredRot = Math.atan2(dy, dx);
        SmartDashboard.putNumber("Desired Rot", Math.toDegrees(desiredRot));
        double rotationFeedback = m_drivetrainSubsystem.calculateRotationFeedback(new TrapezoidProfile.State(MathUtil.angleModulus(desiredRot), 0));
        m_drivetrainSubsystem.driveFieldRelative(
            new ChassisSpeeds(
                m_translationXSupplier.getAsDouble(),
                m_translationYSupplier.getAsDouble(),
                rotationFeedback
            )
        );
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.driveBotRelative(new ChassisSpeeds(0, 0, 0));
    }
}
