package frc.robot.commands.driveCommands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.Constants;
import frc.robot.subsystems.drivetrain.DrivetrainSubsystem;
import java.util.ArrayList;
import java.util.function.DoubleSupplier;

public class StageLockCommand extends Command {
    private Rotation2d m_targetRotation;
    private final DrivetrainSubsystem m_drivetrainSubsystem;
    private final DoubleSupplier m_translationXSupplier;
    private final DoubleSupplier m_translationYSupplier;
    private final ArrayList<Pose2d> chainPoses = new ArrayList<>();

    public StageLockCommand(DrivetrainSubsystem drivetrainSubsystem, DoubleSupplier translationXSupplier, DoubleSupplier translationYSupplier) {
        createChainPoses();
        m_drivetrainSubsystem = drivetrainSubsystem;
        m_translationXSupplier = translationXSupplier;
        m_translationYSupplier = translationYSupplier;
        updateTargetRotation();
        addRequirements(m_drivetrainSubsystem);
    }

    protected void createChainPoses() {
        chainPoses.add(new Pose2d(4.4,3.3,Rotation2d.fromDegrees(60)));
        chainPoses.add(new Pose2d(4.4,4.92,Rotation2d.fromDegrees(-60)));
        chainPoses.add(new Pose2d(5.85,4.11,Rotation2d.fromDegrees(-180)));
        chainPoses.add(new Pose2d(12.11,3.3,Rotation2d.fromDegrees(120)));
        chainPoses.add(new Pose2d(12.11,4.92,Rotation2d.fromDegrees(-120)));
        chainPoses.add(new Pose2d(10.71,4.11,Rotation2d.fromDegrees(0)));
    }

    protected void updateTargetRotation() {
        m_targetRotation = m_drivetrainSubsystem.getEstimatedPose().nearest(chainPoses).getRotation();
    }

    @Override
    public void execute() {
        updateTargetRotation();
        double dr = m_drivetrainSubsystem.calculateRotationFeedback(new TrapezoidProfile.State(MathUtil.angleModulus(m_targetRotation.getRadians()), 0));
        m_drivetrainSubsystem.driveFieldRelative(
            new ChassisSpeeds(
                m_translationXSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND,
                m_translationYSupplier.getAsDouble() * Constants.MAX_VELOCITY_METERS_PER_SECOND,
                dr
            )
        );
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.driveBotRelative(new ChassisSpeeds(0,0,0));
    }

}
