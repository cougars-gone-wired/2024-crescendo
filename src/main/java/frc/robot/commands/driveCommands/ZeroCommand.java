package frc.robot.commands.driveCommands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.drivetrain.DrivetrainSubsystem;

public class ZeroCommand extends Command {
    private DrivetrainSubsystem m_drivetrainSubsystem;


    public ZeroCommand(DrivetrainSubsystem drivetrainSubsystem) {
        m_drivetrainSubsystem = drivetrainSubsystem;
        // don't add the subsystem as a requirement as we don't (necessarily want to stop driving when this command is triggered)
    }

    @Override
    public void execute() {
        m_drivetrainSubsystem.zeroGyroscope();
    }

    @Override
    public boolean isFinished() {
        return true; // essentially an instant command
    }
}