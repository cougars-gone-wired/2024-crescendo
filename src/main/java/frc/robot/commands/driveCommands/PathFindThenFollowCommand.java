package frc.robot.commands.driveCommands;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.path.PathPlannerPath;

import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.Command;

public class PathFindThenFollowCommand {

    private static PathConstraints constraints = new PathConstraints(
        3.0, 4.0,
        Units.degreesToRadians(540), Units.degreesToRadians(720));


    public static Command fromPathFile(String filename) {
        PathPlannerPath ampPath = PathPlannerPath.fromPathFile(filename);
        return AutoBuilder.pathfindThenFollowPath(
            ampPath,  
            constraints,
            0.0); 
    }

}
