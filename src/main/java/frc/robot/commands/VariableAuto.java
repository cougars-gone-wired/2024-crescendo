package frc.robot.commands;

import java.util.List;
import java.util.Map;

import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardComponent;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.shuffleboard.SimpleWidget;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.RunnablePath;

public class VariableAuto {
    private ShuffleboardTab m_shuffleboardTab = Shuffleboard.getTab("Auto Config");
    
    private RunnablePath note0;
    private RunnablePath note1;
    private RunnablePath note2;
    private RunnablePath note3;
    private RunnablePath note4;
    private RunnablePath note5;
    private RunnablePath note6;
    private RunnablePath note7;

    private SimpleWidget note0Widget;
    private SimpleWidget note1Widget;
    private SimpleWidget note2Widget;
    private SimpleWidget note3Widget;
    private SimpleWidget note4Widget;
    private SimpleWidget note5Widget;
    private SimpleWidget note6Widget;
    private SimpleWidget note7Widget;

    private RunnablePath[] pathList = {note0, note1, note2, note3, note4, note5, note6, note7};
    private SimpleWidget[] components = {note0Widget, note1Widget, note2Widget, note3Widget, note4Widget, note5Widget, note6Widget, note7Widget};

    public VariableAuto() {
        
        createNotes();
        createTables();
        createCommandGroup();

    }

    public void createNotes() {
        for(int i = 0; i < pathList.length; i++) {
            pathList[i] = new RunnablePath(i);
        }
    }

    public void createTables() {
        for(int i = 0; i < components.length; i++) {
            components[i] = m_shuffleboardTab.add("Note"+i, true).withWidget(BuiltInWidgets.kToggleSwitch).withPosition(i/4, i%4);
        }
    }

    public void update() {
        for(int i = 0; i < components.length; i++) {
            boolean tmp = components[i].getEntry().getBoolean(false);
            pathList[i].setDesired(tmp);
        }
    }

    public SequentialCommandGroup createCommandGroup() {
        SequentialCommandGroup commandGroup = new SequentialCommandGroup();

        update();
        for(RunnablePath path : pathList) {
            if(path.desired()) {
                commandGroup.addCommands(path.runPath());
            }
        }
        commandGroup.setName("OMAHA");
        return commandGroup;
    }

    public SequentialCommandGroup getCommandGroup() {
        return createCommandGroup();
    }
}
